@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Show all articles</div>

                <div class="card-body">
                    @role('admin')
                        <p><a href="#">Delete a posts</a></p>
                    @endrole

                    @can('edit posts')
                        <a href="#">Edit a post</a>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
